import mongoose from "mongoose";

// const prodSchema = new Schema({ prodId: mongoose.ObjectId, count: Number});
const saleSchema = new mongoose.Schema({
  date: {
    type: Date,
    required: true,
    default: Date.now()
  },
  client: {
    type: mongoose.ObjectId,
    required: true
  },
  products: {
    type: [{ prodId: mongoose.ObjectId, count: Number}],
    required: true,
    default: undefined
  },
  totalSale: {
    type: Number,
    required: true
  },
  saleStatus: {
    type: String,
    required: true,
    default: "pending",
  }
});

const Sale = mongoose.model("Sale", saleSchema);

module.exports = Sale;
