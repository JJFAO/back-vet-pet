import mongoose from 'mongoose';

const featuredSchema = new mongoose.Schema({
  products: {
    type: [{ prodId: mongoose.ObjectId}],
    required: true,
    default: undefined
  }
},
{timestamps:{createdAt: 'created_At'}});

const Featured = mongoose.model('Featured', featuredSchema);

module.exports = Featured;