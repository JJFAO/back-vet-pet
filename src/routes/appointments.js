import express from 'express';
var router = express.Router();
import Appoint from '../models/Appointment';
import moment from 'moment';
import AuthToken from "../middelwares/AuthToken";
import checkRole from "../middelwares/AuthRole";


router.get('/all', AuthToken, checkRole({ hasRole: ["admin"] }), (req, res) => {
  const date = moment(req.params.date).toDate();
  Appoint.find({}, null, (err, appoints) => {
    if (err) {
      res.status(500).send(err)
      return;
    }
    res.send({ appointments: appoints })
  })
})


router.get('/:date', AuthToken, (req, res) => {
  const date = moment(req.params.date).toDate();
  Appoint.find({
    startTime: {
      $gte: date,
      $lte: moment(date).endOf('day')
    }
  }, null, (err, appoints) => {
    if (err) {
      res.status(500).send(err)
      return;
    }
    res.send({ appointments: appoints })
  }).select('startTime -_id')
})


router.get('admin/:date', AuthToken, checkRole({ hasRole: ["admin"] }), (req, res) => {
  // const date = moment(req.params.date).toDate();
  Appoint.find({
    // startTime: {
    //   $gte: date,
    //   $lte: moment(date).endOf('day')
    // }
  }, null, (err, appoints) => {
    if (err) {
      res.status(500).send(err)
      return;
    }
    res.send({ appointments: appoints })
  })
})


router.post('', AuthToken, (req, res) => {
  console.log(req.body.startTime);
  
  Appoint.findOne({ startTime: req.body.startTime }, function (err, resp) {
    if (err) {
      res.status(500).send(err);
      return;
    }
    if (moment(req.body.startTime).toDate() < new Date()) {
      res.status(505).json({ msg: 'Fecha inválida, ingrese una fecha posterior a hoy' });
      return;
    }
    if (!(new Date() <= new Date(req.body.startTime) && moment(req.body.startTime).toDate() <= (moment().add(31, 'days').toDate()))) {
      res.status(506).json({ msg: 'Fecha inválida, ingrese una fecha dentro de este mes' });
      return;
    }
    if (moment(req.body.startTime).day() === 0) {
      res.status(507).json({ msg: 'Fecha inválida, no atendemos los domingos' });
      return;
    }
    if (resp !== null) {
      res.status(502).json({ msg: 'Turno ya reservado, elija otro módulo disponible' });
      return;
    }
    const newAppoint = new Appoint({
      startTime: req.body.startTime,
      description: req.body.description,
      userId: res.locals._id
    })
    newAppoint.save((err, appoint) => {
      if (err) {
        res.send(err);
      } else {
        res.send(appoint);
        console.log('Turno reservado a las: ', appoint);
      }
    })
  });
})


router.delete('/delete/:id', AuthToken, (req, res) => {
  Appoint.findOne({ _id: req.params.id }, function (err, resp) {
    if (err) {
      res.send(err);
      return;
    }
    if (resp == null) {
      res.status(521).json({ msg: "No existe turno con esta fecha" });
      return;
    }
    if (!(resp.userId == res.locals._id || res.locals.role == "admin")) {
      res.status(520).json({ msg: "No puede borrar un turno que no le pertenece" });
      return;
    }
    Appoint.deleteOne({ _id: req.params.id }, function (err) {
      if (err) {
        res.send(err);
        return;
      }
      res.status(210).json({ msg: "Turno eliminado" });
    });
  })
})

export default router;